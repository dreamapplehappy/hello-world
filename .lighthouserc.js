module.exports = {
    ci: {
        collect: {
            url: ['http://localhost:8080/'],
            staticDistDir: './dist',
            // lighthouse settings
            settings: {
                skipAudits: [
                    'metrics/largest-contentful-paint',
                    'accessibility/color-contrast'
                ]
            }
        },
        upload: {
            target: 'temporary-public-storage',
        }
    },
};
